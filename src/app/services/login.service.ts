import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, delay, mergeMap } from 'rxjs/operators';
import { readAllData } from '../Models/readAllData.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = 'https://dummyintegracion.herokuapp.com';

  constructor(private http : HttpClient ) { 
  }
  
  validaAccount=(algo:any):Observable<any>=>{
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    return this.http.post('https://dummyintegracion.herokuapp.com/validaAccount',algo,options )
    .pipe(map(res=>{
      String(res)
    }));
  }

  createAccount=(algo:any):Observable<any>=>{
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    return this.http.post('https://dummyintegracion.herokuapp.com/createAccount',algo,options )
    .pipe(map(res=>{
      String(res)
    }));
  }

  createData=(algo:any):Observable<any>=>{
    const options={
      headers:new HttpHeaders({
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*',
        'Access-Control-Allow-Methods':'GET, POST, DELETE, PUT',
        'Access-Control-Allow-Headers':'X-Requests-With,content-type'
      }),
      observe: 'response' as 'response'
    };
    return this.http.post('https://dummyintegracion.herokuapp.com/createData',algo,options )
    .pipe(map(res=>{
      String(res)
    }));
  }

  getPersonas(usuario: string, categoria: string) {
    const params = new HttpParams({
      fromObject: {
        user: usuario,
        categoria: categoria
      }
    });
    return this.http.get('https://dummyintegracion.herokuapp.com/readAllData', {params: params})
    .pipe(
      map( this.crearArreglo ),
      delay(0)
    );
  }

  private crearArreglo( paisObjeto: any ) {
    const paises: readAllData[] = [];
    if(paisObjeto === null ) {
      return [];
    }
    Object.keys( paisObjeto ).forEach( key => {
      const pais: readAllData = paisObjeto[key];
      //pais.id = key;
      paises.push( pais );
    });
    return paises;
  }

  borrarPersona(id: string = '') {
    console.log('borarr');
    const params = new HttpParams({
      fromObject: {
        id: id
      }
    });
    return this.http.delete('https://dummyintegracion.herokuapp.com/deleteData', {params: params})
    .pipe(map(res=>{
      String(res)
    }));
  }

  borrarPersonas(usuario: string, categoria: string) {
    console.log('borarr todas');
    const params = new HttpParams({
      fromObject: {
        user: usuario,
        categoria: categoria
      }
    });
    return this.http.delete('https://dummyintegracion.herokuapp.com/deleteAllData', {params: params})
    .pipe(map(res=>{
      String(res)
    }));
  }
  
}
