import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  })

  get f() {
    return this.form.controls;
  }

  constructor(private rutas: Router,
    private loginService : LoginService) { }

  ngOnInit(): void {
  }
  
  guardar(){
    const datos = this.form.value;
    console.log(datos);

    this.loginService.createAccount(datos)
    .pipe(timeout(20000))
    .subscribe((resp) => {
      console.log('resp');
      console.log(resp.status);
      this.validaResponse(resp.status, resp);
      //return resp;
    }, response => {
      console.log('response');
      console.log(response.status);
      this.validaResponse(response.status, response);
    })
  }

  validaResponse(code:any, response:any) {
    switch (code) {
      case 200:
        this.rutas.navigate(['/login']);
        break;
      default:
        break;
    }
  }
}
