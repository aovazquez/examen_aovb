import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';
import { validaAccount } from '../Models/validaAccount.model';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  //formi: FormGroup;
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  })

  get f() {
    return this.form.controls;
  }
  
  constructor(
    private rutas: Router,
    private loginService : LoginService) {    
  }

  ngOnInit(): void {
  }
  
  acceder(){
    const datos = this.form.value;
    console.log(datos);
    let body : validaAccount = {email:'aovazquez2@dominio.com', password :'alvaro2'};
    let bodyJson : String;

    bodyJson = JSON.stringify(body);
    console.log(bodyJson);
    


    //this.loginService.validaAccount(bodyJson)
    this.loginService.validaAccount(datos)
    .pipe(timeout(20000))
    .subscribe((resp) => {
      console.log('resp');
      console.log(resp.status);
      this.validaResponse(resp.status, resp);
      //return resp;
    }, response => {
      console.log('response');
      console.log(response.status);
      this.validaResponse(response.status, response);
    })
  }

  validaResponse(code:any, response:any) {
    //this.spinner.hide();
    switch (code) {
      case 200:
        //this.guardService.setLogeado(true);
        this.rutas.navigate(['/dashboard']);
        break;
      default:
        //this.guardService.setLogeado(false);
        //this.toastr.error('Ups', 'Error, valide sus credenciales', {
        //  timeOut: 3000,
        //}
        //);
        //this.form.reset();
        //$('#myModal').modal('toggle');
        break;
    }
  }
  
}
