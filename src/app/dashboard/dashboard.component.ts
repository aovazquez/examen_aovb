import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';
import { readAllData } from '../Models/readAllData.model';
import { LoginService } from '../services/login.service';
import { modeloRegistros } from '../Models/modeloRegistros.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  registros: readAllData[] = [];

  form = new FormGroup({
    user: new FormControl('', [Validators.required, Validators.email]),
    categoria: new FormControl('', [Validators.required])
  })

  modelo : modeloRegistros = new modeloRegistros();
  user1: String = '';
  categoria1: String = '';

  constructor(private rutas: Router,
    private loginService : LoginService) { }

  ngOnInit(): void {
    this.loginService.getPersonas('aovazquez','login')
    .subscribe( resp => {
      this.registros = resp;
    });
  }

  buscarPersonas(){
    this.loginService.getPersonas(String(this.modelo.user),String(this.modelo.categoria))
    .subscribe( resp => {
      this.registros = resp;
    });
  }

  borrarPersona(persona: readAllData, i: number) {
    this.loginService.borrarPersona(String(persona.id))
    .subscribe(resp => {
      console.log('resp');
      //console.log(resp.status);
      //this.validaResponse(resp.status, resp);
      //return resp;
    }, response => {
      console.log('response');
      console.log(response.status);
      this.validaResponse(response.status, response);
    })
    
  }

  borrarPersonas(usuario: string, categoria: string){
    this.loginService.borrarPersonas(String(this.modelo.user),String(this.modelo.categoria))
    .subscribe(resp => {
      console.log('resp');
      //console.log(resp.status);
      //this.validaResponse(resp.status, resp);
      //return resp;
    }, response => {
      console.log('response');
      console.log(response.status);
      this.validaResponse(response.status, response);
    })
    
  }

  validaResponse(code:any, response:any) {
    //this.spinner.hide();
    switch (code) {
      case 200:
        //this.guardService.setLogeado(true);
        this.loginService.getPersonas(String(this.modelo.user),String(this.modelo.categoria))
        .subscribe( resp => {
          this.registros = resp;
        });
        this.rutas.navigate(['/dashboard']);
        break;
      default:
        //this.guardService.setLogeado(false);
        //this.toastr.error('Ups', 'Error, valide sus credenciales', {
        //  timeOut: 3000,
        //}
        //);
        //this.form.reset();
        //$('#myModal').modal('toggle');
        break;
    }
  }

}
