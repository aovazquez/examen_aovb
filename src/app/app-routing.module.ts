import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarPersonaComponent } from './agregar-persona/agregar-persona.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditarPersonaComponent } from './editar-persona/editar-persona.component';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'agregar', component: AgregarPersonaComponent },
  { path: 'editar', component: EditarPersonaComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
