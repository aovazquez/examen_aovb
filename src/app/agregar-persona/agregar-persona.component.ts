import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';
import { modeloRegistros } from '../Models/modeloRegistros.model';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.component.html',
  styleUrls: ['./agregar-persona.component.css']
})
export class AgregarPersonaComponent implements OnInit {

  modelo : modeloRegistros = new modeloRegistros();

  constructor(private rutas: Router,
    private loginService : LoginService) { }

  ngOnInit(): void {
  }

  guardar(){
    this.loginService.createData(JSON.stringify(this.modelo))
    .pipe(timeout(20000))
    .subscribe((resp) => {
      console.log('resp');
      console.log(resp.status);
      this.validaResponse(resp.status, resp);
      //return resp;
    }, response => {
      console.log('response');
      console.log(response.status);
      this.validaResponse(response.status, response);
    })
  }

  validaResponse(code:any, response:any) {
    //this.spinner.hide();
    switch (code) {
      case 200:
        //this.guardService.setLogeado(true);
        this.rutas.navigate(['/dashboard']);
        break;
      default:
        //this.guardService.setLogeado(false);
        //this.toastr.error('Ups', 'Error, valide sus credenciales', {
        //  timeOut: 3000,
        //}
        //);
        //this.form.reset();
        //$('#myModal').modal('toggle');
        break;
    }
  }

}
